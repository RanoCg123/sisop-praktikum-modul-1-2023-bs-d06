# sisop-praktikum-modul-1-2023-BS-D06

# Anggota
5025211119	Gilang Aliefidanto

5025211056	I Gusti Agung Ngurah Adhi Sanjaya

5025211185	Rano Noumi Sulistyo
## Soal 1
Penjelasan Soal 1
### Kode
```
#!/bin/bash
echo "Point 1"
awk -F "," '/Japan/ {print}' Urankings.csv | head -n 5 > sortingjapan.csv
awk -F "," '{print $1,$2}' sortingjapan.csv
echo "Point 2"
awk -F "," '{print $9,$2}' sortingjapan.csv | sort
echo "Point 3"
awk -F "," '/Japan/ {print $20,$2}' Urankings.csv | sort -n | head -10
echo "Point 4"
awk -F "," '/Keren/ {print $2}' Urankings.csv
```


Pada Line No 2, 5, 7, 9, itu hanya mengeluarkan output untuk memisahkan point dari soal 1

### POINT 1 :
Pada line 3 itu terdapat awk yang berfungsi untuk mengambil catatan atau data dari file Urankings.csv, selanjutnya itu terdapat -F “,” yang berfungsi untuk memberitahu program bahwa text setelah “,” merupakan text yang berbeda dari sebelum “,” jadi seperti ada 2 kolom pada satu baris.

Selanjutnya itu ada ‘/Japan/ {print}’ Urankings.csv yang fungsinya untuk mengeluarkan output pada file Urankings.csv dan di filter menggunakan kata kunci “Japan”

Selanjutnya ada head -n 5 > sortingjapan.csv yang dimana head -n 5 itu artinya program akan mengambil data hanya 5 teratas dari data yang sudah di sortir pada ‘/Japan/ {print}’ Urankings.csv, dan data tersebut akan di tempatkan pada temporary file yaitu sortingjapan.csv

###ISI DARI HASIL SORT DI TEMPORARY FILE .csv
```
23,The University of Tokyo,JP,Japan,100,7,99.7,9,91.9,78,73.3,128,10.4,601+,27.8,448,89.5,174,97.8,33,85.3
36,Kyoto University,JP,Japan,98.6,21,98.9,15,94.8,62,54.2,234,14.9,601+,22.1,503,85.5,233,56.9,201,81.4
55,Tokyo Institute of Technology (Tokyo Tech),JP,Japan,74.1,80,93.4,42,81.5,126,65.9,168,36.1,433,37.9,362,55.8,601+,33.6,377,72.5
68,Osaka University,JP,Japan,80.2,68,85.4,61,67.4,205,59.1,206,25,525,14.4,601+,75.2,385,21,567,68.2
79,Tohoku University,JP,Japan,71.8,86,78.1,75,98.6,43,34.2,401,14.1,601+,16.4,595,66.8,493,18.7,601+,64.9
```

Selanjutnya ada awk -F "," '{print $1,$2}' sortingjapan.csv yang dimana line ini akan melakukan pengambilan data pada sortingjapan.csv yang sudah di sorting sebelumnya pada $1 (kolom pertama) dan $2 (kolom kedua).

### Point 2 :
Pada line 6 program akan melakukan sort dari paling rendah pada $9 (kolom ke 9, kolom fsr score) dan $2 (kolom ke 2, kolom nama universitas) 

### Point 3 :
Pada line 8, program akan melakukan sort pada kolom ke 20 (ger rank) dan kolom ke 2 (nama universitas)  pada kata kunci Japan di file Urankings.csv. Fungsi sort -n adalah untuk mengurutkan dari terkecil hingga terbesar secara numerik dan mengambil 10 hasil teratas dari hasil sort -n menggunakan head -10 karena ranking itu kalau nilainya kecil maka itu rank yang lebih tinggi
### Point 4 :
Pada Line 10, program akan melakukan pencarian kata kunci “Keren” pada kolom ke 2 (nama universitas) pada file Urankings.csv 

### Hasil Output : 
```
Point 1
23 The University of Tokyo
36 Kyoto University
55 Tokyo Institute of Technology (Tokyo Tech)
68 Osaka University
79 Tohoku University
Point 2
67.4 Osaka University
81.5 Tokyo Institute of Technology (Tokyo Tech)
91.9 The University of Tokyo
94.8 Kyoto University
98.6 Tohoku University
Point 3
33 The University of Tokyo
90 Keio University
127 Waseda University
169 Hitotsubashi University
188 Tokyo University of Science
201 Kyoto University
367 Nagoya University
377 Tokyo Institute of Technology (Tokyo Tech)
379 International Christian University
543 Kyushu University
Point 4
Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren)
```
## Soal 2
Kita diminta untuk melakukan 3 hal utama
- setiap 10 jam kita perlu menarik foto dari internet bertema indonesia sebanyak jam saat itu
- foto foto tersebut dimasukkan ke sebuah folder baru untuk tiap kali menarik
- setiap hari intuk menghemat memori semua kumpulan yang ada di zip 
Oleh karena itu kita buat kode :
```
#!/bin/bash
# crontab
# 0 */10 * * * kobeni_liburan.sh coping
# @daily kobeni_liburan.sh devil
# jika @daily todak bisa maka 0 0 * * * kobeni_liburan.sh devil

if [ "$1" == "coping" ]; then
	num=1
	until [ ! -d kumpulan_$num ];do
    	num=$((num+1))
	done
	mkdir kumpulan_$num
	cd kumpulan_$num
	chour=$((10#"$(date +%H)"))
	if [ chour = 0 ]; then
	chour=1
	fi
	for ((t=1; t <= chour; t++))
	do
		wget -O perjalan_"$t".jpg https://www.nationsonline.org/gallery/Indonesia/Piaynemo-West-Papua.jpg
	done
elif [ "$1" == "devil" ]; then
	k=1
	until [ ! -f devil_$k.zip ];do
    		k=$((k+1))
	done
	i=1
	until [ ! -d kumpulan_$i ]; do
    		zip -r devil_$k.zip kumpulan_$i
    		rm -r kumpulan_$i
    		i=$((i+1))
	done 
fi
```
Agar bisa membedakan untuk yang mengzip dan menarik foto maka kita bagi menjadi "coping" dan "devil", dalam coping kita akan menarik foto dan membuat folder berisi foto tersebut dan dalam devil kita kan menzipnya.
Ketika "coping" pertama kita cek apakah sudah ada folder kumpulan. kita perlu memastikan kita membuat folder baru. kita loop num++ sampai tidak ada kumpulan_num. setelah tidak ada kita mkdir dan cd untuk masuk directory. Dari sini mudah kita hanya perlu mengecek jam saat ini menggunakan $date. Bisa dilihat saya menggunakn 10# hal ini agar kita menulis dalam bentuk integer base 10. Salah satu kondisi lain adalah jika jam = 00 maka kita hanya perlu menarik 1 kali. 
Selanjutnya kita akan melakukan loop penarikan foto. disini kita menggunakan wget -O yang akan mendownload file spesifik link.
Ketika "devil" kita akan pertama mengecek bila ada zip devil yang ada. kita harus memastikan zip tidak tumpuk dan setiap devil dijalankan terdapat zip baru yang dibuat. Lalu kita akan mengecek apakah ada file kumpulan. Jika tidak ada berarti tidak perlu membuat zip. jika ada kita akan melakukan loop untuk memasukkan folder kumpulan ke zip sampai tidak ada lagi. Nah salah satu hal penting dalam proses ini adalah kita harus mendelet kumpulan yang telah di zip agar sesuai keinginan kobeni menghemat memori.

## Soal 3
### Penjelasan Umum
Pada soal, diketahui terdapat dua file script bernama louis.sh dan retep.sh. Masing-masing memiliki peran, louis.sh untuk melakukan register, sedangkan retep.sh untuk melakukan login. Setiap percobaan login ataupun register, akan tercatat pada file log.txt dengan format:
```
echo "yy/mm/dd hh:mm:ss [message]" >> log.txt
```

#### Script Register (louis.sh)
Pada register, terdapat banyak syarat untuk melakukn registrasi pengguna yang akan disimpan ke dalam file users/users.txt. Jika syarat tidak terpenuhi, maka akan dilakukan looping hingga username dan password sesuai dengan syarat. Syarat yang dilakukan agar pengguna dapat melakukan registrasi adalah:
- Username baru tidak boleh sama dengan username sebelumnya
- Password minimal memiliki 8 karakter yang terdiri dari setidaknya 1 huruf kapital dan huruf kecil
- Password tidak boleh terdapat karakter selain *alphanumeric*
- Password tidak boleh sama dengan username
- Password tidak boleh mengandung kata "chicken" dan "ernie"
(untuk password kami menambahkan log error supaya jelas letak kesalahan pada menulis password)

__Format users/users.txt__
```
[username1]
[password1]
[username2]
[password2]
...
```
__daftar [message]__
```

1 Username sama dengan username sebelumnya
  REGISTER: ERROR User already exists
2 Password kurang dari 8 karakter
  REGISTER: ERROR Password must at least 8 character
3 Password tidak terdapat huruf kapital dan/atau huruf kecil
  REGISTER: ERROR Password must contains at least 1 uppercase and lowercase
4 Password terdapat karakter spesial
  REGISTER: ERROR Password must contains alphanumeric
5 Password sama dengan user
  REGISTER: ERROR Password must not be same as username
6 Password mengandung kata "chicken" atau "ernie"
  REGISTER: ERROR password must not contain both chicken and ernie word
7 Username dan password telah memenuhi syarat
  REGISTER: INFO user [username] registered successfully
```

__Script lengkap__
```
#!/bin/bash

echo "Register"
username=''
error_status_0=1
while [ "$error_status_0" != 0 ]
do
  echo "username:"
  read username
  error_status_0=0
  while read user_list; read password_user
  do
    if [ "$username" == "$user_list" ] #syarat1
    then
      datetime0=$(date +"%y/%m/%d %T")
      echo "$datetime0 REGISTER: ERROR User already exists" #log1 >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
      error_status_0=1
    fi
  done <sisop-praktikum-modul-1-2023-BS-D06/soal3/users/users.txt #fileinput-untuk-cek-user-sebelumnya
  echo ""
done
password=''
error_status_1=1
while [ "$error_status_1" != 0 ]
do
  echo "password:"
  read password
  datetime=$(date +"%y/%m/%d %T")
  if [ "${#password}" -lt 8 ] #syarat2
  then
    echo "$datetime REGISTER: ERROR password must at least 8 character" #log2 >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  elif [[ "$password" != *[QWERTYUIOPASDFGHJKLZXCVBNM]* ]] || [[ "$password" != *[qwertyuiopasdfghjklzxcvbnm]* ]] #syarat3
  then
    echo "$datetime REGISTER: ERROR password must contains at least 1 uppercase and lowercase" #log3 >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  elif [[ "$password" == *[!QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890]* ]] #syarat4
  then
    echo "$datetime REGISTER: ERROR password must contains alphanumeric" #log4 >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  elif [ "$password" == "$username" ] #syarat5
  then
    echo "$datetime REGISTER: ERROR password must not be same as username" #log5 >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  elif [[ "$password" == *"chicken"* ]] || [[ "$password" == *"ernie"* ]] #syarat6
  then
    echo "$datetime REGISTER: ERROR password must not contain both chicken and ernie word" #log6 >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  else
    error_status_1=0
  fi
  echo ""
done
echo -e "$username\n$password" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/users/users.txt
datetime1=$(date +"%y/%m/%d %T")
echo "$datetime1 REGISTER: INFO user $username registered successfully" #log7 >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt

```

#### Script login (retep.sh)
Script login cukup simpel. Pengguna harus menulis username (jika sudah terftar) yang sesuai. Setelah itu script akan mencari password yang telah tersimpan pada file users/users.txt. Jika sudah akan tersimpan kemudian script meminta pengguna untuk memasukan password untuk melakukan login
```
- Password benar
  LOGIN: INFO User [username] logged in
- Password salah
  LOGIN: ERROR Failed login attempt on user [username]
```

__Script lengkap__
```
#!/bin/bash

echo "Login"
echo "Username:"
read username
password_actual=''
#cari username di users/users.txt
while read user_list; read password_user
do
  if [ "$username" == "$user_list" ] #jika-ketemu
  then
    password_actual=$password_user #copy-paste-ke-variabel
  fi
done <sisop-praktikum-modul-1-2023-BS-D06/soal3/users/users.txt

error_status_0=1
while [ "$error_status_0" != 0 ]
do
  echo "Password:"
  read password
  echo ""
  datetime=$(date +"%y/%m/%d %T")
  if [ "$password" == "$password_actual" ] #jika-password-benar
  then
    echo "$datetime LOGIN: INFO User $username logged in" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
    error_status_0=0
  else #jika-password-salah
    echo "$datetime LOGIN: ERROR Failed login attempt on user $username">>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  fi
done
```
## Soal 4
Dalam soal ini kita diminta untuk melakukan caesar cipher ke kanan apfabet sebanyak jam dijalankannya kode terhadap syslogkita tentukan poin poin yang perlu diperhatikan
- caesar cipher
- berdasarkan waktu sekarang
- terdapat drcrypt juga
- mengakses syslog
- nama sesuai date dengan format jam:menit tanggal:bulan:tahun.txt
- dilakukan setiap 2 jam
### Kode encrypt :
```
#!/bin/bash 
# cron job = 0 */2 * * *
# jam saai ini
chour=$(date +%H)
# nama file
name=$(date "+%H":"%M %d":"%m":"%y")
# string alfabet untuk upper dan lowercase. dibedakan karena tr case sensitive.
# string panjangnya 2 kali agar bisa diambil substring untuk cipher. seperti memindahkan pointer.
huruf="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
upper="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
# fungsi tr yang mengubah semua yang ada di set 1 ke set 2. ukurannya disamakan 26 sesuai alfabet
str=$(cat /var/log/syslog | tr "${huruf:0:26}" "${huruf:${chour}%26:26}" )
# untuk yang upppercase dan di masukkan ke file sesuai name
echo "$str" |tr "${upper:0:26}" "${upper:${chour}%26:26}" > "$name".txt
```
Disini kami menggunkan 2 poin utama, yaitu tr dan substring untuk membuat caesar cipher. tr atau translate akan mengubah set 1 menjadi set 2. sebagai contoh 
```
$ echo sisopd | tr [:lower:] [:upper:]
```
command diatas akan menguab sisopd menjadi SISOPD. hal ini dikarenakan oleh tr yang melakukan "translasi" dari lowecase ke uppercase.
Kita akan menggunkan hal ini untuk mengubah alfabet lowercase maupun uppercase menjadi alfabet sesuai dengan shift caesar chiper yang berdasarkan jam.
Hal ini bisa dilakukan dengan menggunkan sistem substring string:number1:number2 yang akan menganbil substring dari number1 sebanyak number2. contoh
```
az=praktikumsisop
echo "${az:6:5}"
```
Kode diatas akan mengambil char ke 6 sepanjang 5 kali dari praktikumsisop sehingga akan mengeluarkan substring berupa kumsi. Dengan kedua command ini kita akan melakukan encryption caesar cipher berdasarkan jam
pertama kita akan menggunakan command date untuk menentukan jam saat itu dan dimasukkan sebagai variabel sehingga bisa digunakan sebagai anggka. 
lalu kita melakukan date sebagai variable untuk nama file sesuai template.
setelah itu kita akan mendeklarasi string untuk masing masing alfabet uppercase dan lowercase. dibedakan agar tr tidak kebingunan. jika digabung maka kita harus menggeser 2 kalo lebih banyak dan substring lebih rumut. variable berisi string panjangnya dobel dikarenakan kita akan dalam penggeseran bisa melebihi 26 yaitu jumlah alfaber selebihnya kita harus ambil mulai dari a lagi.
akhirnya kita akan melakukan proses terakshir dimana kita meread syslog menggunakan cat dan di translate. Kita ambil substring sebanyak 26 dari 0 untuk set yang ditranslate. dan sebanyak 26 dari jam dimana program dijalankan. hal ini akan membuat substring yang dimulai dari huruf setelah a dilakukan pergesaran untuk caesar cipher
### contoh :
```
jam = 10
huruf="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
upper="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
str=$(echo Praktikum Sisop Modul 1 | tr "${huruf:0:26}" "${huruf:10:26}" )
echo "$str" |tr "${upper:0:26}" "${upper:10:26}" 
```
### output = Zbkudsuew Cscyz Wynev 1
diketahui p adalah alfabet ke 16 setelah digeser 10 maka akan menjadi z sesuai output
dengan semua ini maka bisa dibuat program encypt caesar cipher untuk syslog.
untuk bagian terakhir program dijalankan setiap 2 jam sehingga menggunakn cron job dengan konfigursi 0 */2 * * * yang berarti lakukan setiap 2 jam di menit 0.
### kode decrypt :
```
#!/bin/bash 
# read nama file, karena format "+%H":"%M %d":"%m":"%y" perlu $1 dan $2 dibagi " "
name="$1 $2"
# menggunakan expr untuk mengubah string dari nama file yang menunjukkan jam ke int
chour=$( expr "${name:0:2}")
# urutan alfabet seperti encrypt
huruf="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
upper="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
# menngunakan tr lagi namun dibalik agar yang sebelumnya diubah kembali
str=$(cat "$name" | tr "${huruf:${chour}%26:26}" "${huruf:0:26}" )
echo "$str" |tr  "${upper:${chour}%26:26}" "${upper:0:26}" > "$name"_decrypted.txt
```
Untuk decrypt dalam proses tr dan subtring sama dengan encrypt hanya dibalik. bagian rumit dari decrypt adalah bagaimana kita mendapatkan variabel berisi nilai jam yang ada dalam file.
disini kita akan menggunakn comman $1 dan $2 yaitu command yang akan membaca string yang dimasukkan sebagai argumen ketika menjalankan kode. $1 membaca string pertama dan $2 membaca string kedua. " " ada karena dalam format file terdapat space yang membagi string menjadi 2 dan kita perlu membaca 2 2nya untuk mengetahui file yang didecrypt. Hal penting lagi yang perlu diperhatikan adalah penggunaan expr. expr dapat mengubah string menjadi integer. hal ini diperlukan karena kita memerlukan integer untuk command substring. dikarenakan jam ditulis dalam format 00 kita perlu mengambil 2 nilai. 
setelah itu kita melakukan semua seperti encrypt.

