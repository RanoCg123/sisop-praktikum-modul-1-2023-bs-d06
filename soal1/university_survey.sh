#!/bin/bash
echo "Point 1"
awk -F "," '/Japan/ {print}' Urankings.csv | head -n 5 > sortingjapan.csv
awk -F "," '{print $1,$2}' sortingjapan.csv
echo "Point 2"
awk -F "," '{print $9,$2}' sortingjapan.csv | sort
echo "Point 3"
awk -F "," '/Japan/ {print $20,$2}' Urankings.csv | sort -n | head -10
echo "Point 4"
awk -F "," '/Keren/ {print $2}' Urankings.csv
