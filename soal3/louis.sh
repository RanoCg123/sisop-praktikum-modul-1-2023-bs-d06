#!/bin/bash

echo "Register"
username=''
error_status_0=1
while [ "$error_status_0" != 0 ]
do
  echo "username:"
  read username
  error_status_0=0
  while read user_list; read password_user
  do
    if [ "$username" == "$user_list" ]
    then
      datetime0=$(date +"%y/%m/%d %T")
      echo "$datetime0 REGISTER: ERROR User already exists" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
      error_status_0=1
    fi
  done <sisop-praktikum-modul-1-2023-BS-D06/soal3/users/users.txt
  echo ""
done
password=''
error_status_1=1
while [ "$error_status_1" != 0 ]
do
  echo "password:"
  read password
  datetime=$(date +"%y/%m/%d %T")
  if [ "${#password}" -lt 8 ]
  then
    echo "$datetime REGISTER: ERROR password must at least 8 character" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  elif [[ "$password" != *[QWERTYUIOPASDFGHJKLZXCVBNM]* ]] || [[ "$password" != *[qwertyuiopasdfghjklzxcvbnm]* ]]
  then
    echo "$datetime REGISTER: ERROR password must contains at least 1 uppercase and lowercase" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  elif [[ "$password" == *[!QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890]* ]]
  then
    echo "$datetime REGISTER: ERROR password must contains alphanumeric" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  elif [ "$password" == "$username" ]
  then
    echo "$datetime REGISTER: ERROR password must not be same as username" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  elif [[ "$password" == *"chicken"* ]] || [[ "$password" == *"ernie"* ]]
  then
    echo "$datetime REGISTER: ERROR password must not contain both chicken and ernie word" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  else
    error_status_1=0
  fi
  echo ""
done
echo -e "$username\n$password" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/users/users.txt
datetime1=$(date +"%y/%m/%d %T")
echo "$datetime1 REGISTER: INFO user $username registered successfully" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
