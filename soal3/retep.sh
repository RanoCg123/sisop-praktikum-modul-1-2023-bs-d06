#!/bin/bash

echo "Login"
echo "Username:"
read username
password_actual=''
while read user_list; read password_user
do
  if [ "$username" == "$user_list" ]
  then
    password_actual=$password_user
  fi
done <sisop-praktikum-modul-1-2023-BS-D06/soal3/users/users.txt

error_status_0=1
while [ "$error_status_0" != 0 ]
do
  echo "Password:"
  read password
  echo ""
  datetime=$(date +"%y/%m/%d %T")
  if [ "$password" == "$password_actual" ]
  then
    echo "$datetime LOGIN: INFO User $username logged in" >>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
    error_status_0=0
  else
    echo "$datetime LOGIN: ERROR Failed login attempt on user $username">>sisop-praktikum-modul-1-2023-BS-D06/soal3/log.txt
  fi
done
