#!/bin/bash 
# read nama file, karena format "+%H":"%M %d":"%m":"%y" perlu $1 dan $2 dibagi " "
name="$1 $2"
# menggunakan expr untuk mengubah string dari nama file yang menunjukkan jam ke int
chour=$(expr "${name:0:2}")
# urutan alfabet seperti encrypt
huruf="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
upper="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
# menngunakan tr lagi namun dibalik agar yang sebelumnya diubah kembali
str=$(cat "$name" | tr "${huruf:${chour}%26:26}" "${huruf:0:26}" )
echo "$str" |tr  "${upper:${chour}%26:26}" "${upper:0:26}" > "$name"_decrypted.txt

