#!/bin/bash 
# cron job = 0 */2 * * * log_encrypt.sh
# jam saai ini
chour=$(date +%H)
# nama file
name=$(date "+%H":"%M %d":"%m":"%y")
# string alfabet untuk upper dan lowercase. dibedakan karena tr case sensitive.
# string panjangnya 2 kali agar bisa diambil substring untuk cipher. seperti memindahkan pointer.
huruf="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
upper="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
# fungsi tr yang mengubah semua yang ada di set 1 ke set 2. ukurannya disamakan 26 sesuai alfabet
str=$(cat /var/log/syslog | tr "${huruf:0:26}" "${huruf:${chour}%26:26}" )
# untuk yang upppercase dan di masukkan ke file sesuai name
echo "$str" |tr "${upper:0:26}" "${upper:${chour}%26:26}" > "$name".txt

